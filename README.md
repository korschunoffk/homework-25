# homework 25

**Postfix+ dovecot**

1. Поднималась одна виртуальная машина с адресом 192.168.10.101
2. Установлен FQDN mail.postfixtest.com
3. Настройки производились по этой how to
 `https://www.linode.com/docs/email/postfix/email-with-postfix-dovecot-and-mariadb-on-centos-7/`

За исключением certsbot - для генерации сертификатов был использован openssl  
```
# mkdir /etc/postfix/certs
# openssl req -new -x509 -days 3650 -nodes -out /etc/postfix/certs/cert.pem -keyout /etc/postfix/certs/key.pem
```
4. в /etc/hosts на хостовой машине был внесена запись `192.168.10.101   mail.postfix.com`
5. На хостовой машине были установлены telnet и sylpheed - почтовый клиент.
6. Почтовый клиент был сконфигурирован след образом
IMAP: mail.postfixtest.com 993
SMTP: mail.postfixtest.com 587 
7. Было отправлено письмо c почтового ящика email1@postfixtest.com c почтового клиента (соответственно оно и пришло на клиент)
8. Было отправлено письмо с хоста на vagrant машину через telnet

```
telnet mail.postfixtest.com 25

ehlo mail.postfixtest.com 

mail from: email1@postfixtest.com
rcpt to: email1@postfix.com
data
from: email1 <email1@postfixtest.com><
to: email1 < email1@postfixtest.com >
Subject: mail from host to vagrant via Telnet
This is test
.
```

Скриншоты и конфиги находятся в соответствующих папках